import Vue from 'vue'
import createPersistedState from 'vuex-persistedstate'  
import Vuex from 'vuex'

Vue.use (Vuex)
const store = new Vuex.Store({
    plugins: [createPersistedState()],
    state: {
        count: 0,
        user: null,
    },
    mutations: {
        setCount: (state, count) => state.count = count,
        setUser: (state, user) => state.user = user,
    },
    getters: {
        getCount: (state) => 
        {
            return state.count
        },
        getUser: (state) =>
        {
            return state.user
        }
    }
})
export default store;