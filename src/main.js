import Vue from 'vue';
import VueRouter from 'vue-router';

import App from './components/App.vue';
import PageCarFind from './components/PageCarFind.vue';
import PageLogin from './components/PageLogin.vue';
import PageRegistr from './components/PageRegistr.vue';
import PageCarsInfo from './components/PageCarsInfo.vue';
import store from './store.js';

const routes = [
    { path: '/', component: PageCarFind, meta: {requiresAuth:true} },
    { path: '/info/:searchnumb', component: PageCarsInfo , meta: {requiresAuth:true},  props: true },
    { path: '/login/', component: PageLogin },
    { path: '/registr/', component: PageRegistr },
];

const router = new VueRouter({
    routes
});

Vue.use(store);
Vue.use(VueRouter);

new Vue({
    render: h => h(App),
    el: '#app',
    router,
    store,
});