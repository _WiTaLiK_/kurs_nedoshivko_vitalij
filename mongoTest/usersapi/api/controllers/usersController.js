'use strict';


var mongoose = require('mongoose'),
  Task = mongoose.model('Users');

exports.list_all_users = function (req, res) {
  Task.find({}, function (err, users) {
    if (err)
      res.send(err);
    res.json(users);
  });
};



exports.list_filt_user = function(req, res){
    Task.find({"email" : req.params.email},function(err, user){
    if (err)
      res.send(err);
    res.json(user);
    // console.log(req.params.email);
  } );
}


exports.create_a_user = function (req, res) {
  var new_user = new Task(req.body);
  new_user.save(function (err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

/*
exports.read_a_task = function (req, res) {
  Task.findById(req.params.taskId, function (err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

*/
exports.update_a_user = function (req, res) {
  Task.findOneAndUpdate({ _id: req.params._id }, req.body, { new: true }, function (err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};


exports.delete_a_user = function (req, res) {
  Task.remove({
    _id: req.params._id  }, function (err, user) {
    if (err)
      res.send(err);
    res.json({ message: 'User successfully deleted' });
  });
};