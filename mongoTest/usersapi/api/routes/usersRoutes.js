'use strict';
module.exports = function(app) {
  var todoList = require('../controllers/usersController');

  // todoList Routes
  app.route('/users')
    .get(todoList.list_all_users);
  app.route('/user')
    .post(todoList.create_a_user);
    //app.route('/update')
   // .get(todoList.update_data);

  app.route('/user/:email')
   //.get(todoList.read_a_task)
    .get(todoList.list_filt_user);
    
    app.route('/user/del/:_id')
    .delete(todoList.delete_a_user);
    app.route('/user/put/:_id')
    .put(todoList.update_a_user);

    var https = require('https');
    var fs = require('fs');

};
