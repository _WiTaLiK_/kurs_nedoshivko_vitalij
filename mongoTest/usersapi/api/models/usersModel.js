'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var UsersSchema = new Schema({
 
  "email":{
    type:String,
    required: 'Kindly enter the email of the users'
  },
  "password":{
    type:String,
    required: 'Kindly enter the password of the users'
  },
  "type":{
    type:String,
    required: 'Kindly enter the type of the users'
  },
  "search":{
    type:String,
    default: "",
  },
});

module.exports = mongoose.model('Users', UsersSchema);