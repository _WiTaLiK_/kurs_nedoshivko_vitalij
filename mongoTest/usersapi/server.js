var express = require('express'),
  app = express(),
  port = process.env.PORT || 3030,
  mongoose = require('mongoose'),
  Task = require('./api/models/usersModel'), //created model loading here
  bodyParser = require('body-parser');

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});
// mongoose instance connection url connection

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/Users');


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var routes = require('./api/routes/usersRoutes'); //importing route
routes(app); //register the route


app.listen(port);


console.log('users RESTful API server started on: ' + port);