var express = require('express'),
  axios = require('axios'),
  app = express(),
  port = process.env.PORT || 3000,
  mongoose = require('mongoose'),
  Task = require('./api/models/todoListModel'), //created model loading here
  bodyParser = require('body-parser');


app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});
// mongoose instance connection url connection

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/cars');




app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var routes = require('./api/routes/todoListRoutes'); //importing route
routes(app); //register the route

axios.get("https://data.gov.ua/api/3/action/package_show?id=2a746426-b289-4eb2-be8f-aac03e68948c").then(response => {
  let id = response.data.result.resources.pop().id;  
  axios.get("https://data.gov.ua/api/3/action/resource_show?id="+ id).then(response1 => {
    let url = response1.data.result.resource_revisions[0].url;
    var https = require('https'), 
    fs = require('fs'); 
    var fStreamOut = fs.createWriteStream('../mongodb/mvswantedtransport-1.json');
    https.get(url,response => response.pipe(fStreamOut)); 
  })
})

app.listen(port);



// console.log("loading...");
//   var https = require('https'), 
//   fs = require('fs'); 
// var fStreamOut = fs.createWriteStream('../mongodb/mvswantedtransport-1.json');
// https.get('https://data.gov.ua/dataset/9b0e87e0-eaa3-4f14-9547-03d61b70abb6/resource/06e65b06-3120-4713-8003-7905a83f95f5/download/mvswantedtransport_1.json', 
// response => response.pipe(fStreamOut)); 

/*var json = require('../mongodb/mvswantedtransport-1.json');
mongoose.cars.insertMany(json);
*/




console.log('cars list RESTful API server started on: ' + port);