'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var TaskSchema = new Schema({
  "_id":String,
  "ID":String,
  "ODV":String,
  "BRAND":String,
  "COLOR":String,
  "VEHICLENUMBER":String,
  "BODYNUMBER":String,
  "CHASSISNUMBER":String,
  "ENGINENUMBER":String,
  "THEFT_DATA":String,
  "INSERT_DATE":String,
});

module.exports = mongoose.model('Cars', TaskSchema);